$(window).on('load', function() {
    const qr_codes = 'qr_codes.ods';
    var hash = window.location.hash.substring(1);

    if (hash) {
        var qr_text = new Object();
        qr_text.QRId = hash;
        qr_text.QRType = 'Location';
        qr_text.LocationStatus = false;

        $('#qrcode').qrcode({
            render	: 'canvas',
            width   : 512,
            height  : 512,
            ecLevel : 'H',
            mode    : 2,
            text	: JSON.stringify(qr_text)
        });

        fetch(qr_codes).then(function (response) {
            response.arrayBuffer().then(function(file) {
                var workbook = XLSX.read(file);
                var worksheet = workbook.Sheets[workbook.SheetNames[0]];
                var qr_codes = XLSX.utils.sheet_to_json(worksheet);
                var qr_code = qr_codes.find(row => row.qrid == hash);
                var label = qr_code.label;

                document.title = label;
                $('.label')[0].innerHTML = `${label}<br/>Terminal ${qr_code.terminal} ${qr_code.type}`;
            });
        });        
    }
});